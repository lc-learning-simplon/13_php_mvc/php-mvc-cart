@extends('layouts')

@section('content')
<section class="container">
	{{--
			Affiche toute les information d'un seul produit
			avec un bouton pour ajouter le produits et on peut choisir la quantité
		--}}

	<div class="column">
		<img src="{{ $product->picture }}">
		<p class="title is-3">{{ $product->name }}</p>
		<p>Prix : {{ $product->price }} €</p>
		<hr>
		<form action="/cart/add" method="POST">
			<input type="hidden" name="product_id" value="{{ $product->id }}">
			<div class="field has-addons has-addons-left">
				<p class="control">
					<span class="select">
						<select name="quantity" id="quantity">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
						</select>
					</span>
				</p>
				<p class="control">
					<button type="submit" class="button is-link is-light" >Ajouter au panier</button>
				</p>
			</div>
		</form>
	</div>

</section>
@endsection