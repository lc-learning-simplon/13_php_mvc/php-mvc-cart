@extends('layouts')

@section('content')
	<section class="hero is-info is-large">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">Four Oh Four</h1>
				<h2 class="subtitle">
					There's Nothing to see here <a href="/">Go Back</a>
				</h2>
			</div>
		</div>
	</section>
@endsection