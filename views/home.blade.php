@extends('layouts')

@section('content')
<section class="container">
	<h1 class="title">Listes des produits</h1>
	<hr>
	<div class="columns is-desktop">

		@foreach ($products as $product)
			<div class="card" style="margin: 0.5rem;">
				<div class="card-image">
					<img src="{{ $product->picture }}" alt="">
				</div>
				<div class="card-header">
					<div class="card-header-title">
						<p>{{ $product->name }}</p>
					</div>
					<div class="card-content">
						<p>Prix : {{ $product->price }} €</p>
					</div>
				</div>
					<div class="card-footer">
						<a href="/product/{{ $product->id }}" class="button is-link is-light is-fullwidth">En savoir plus</a>
					</div>
			</div>
		@endforeach
		
			{{-- Boucles pour récupérer des produits (https://laravel.com/docs/5.8/blade), 
					 Bulma : https://bulma.io/documentation/columns/responsiveness/,
									 https://bulma.io/documentation/components/card/
			 --}}

		</div>
</section>
@endsection