@extends('layouts')

@section('content')
<section class="container">

    <div class="columns">

        <div class="column is-two-fifths">
            <h2 class="title is-3">Vos coordonnées</h2>
            <hr>
            <form action="/order/add" method="POST">
                <div class="field">
                    <label class="label">Nom</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="Nom" name="last_name" require>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Prénom</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="Prénom" name="first_name" require>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Adresse</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="Adresse" name="address" require>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Code postal</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="Code postal" name="postcode" require>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Téléphone</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="0102030405" name="phone" require>
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        <label class="checkbox">
                            <input type="checkbox">
                            I agree to the <a href="#">terms and conditions</a>
                        </label>
                    </div>
                </div>

                <div class="field is-grouped">
                    <div class="control">
                        <button type="submit" class="button is-link">Suivant</button>
                    </div>
                    <div class="control">
                        <a href="/cart" class="button is-link is-light">Annuler</a>
                    </div>
                </div>
            </form>
        </div>
</section>
@endsection