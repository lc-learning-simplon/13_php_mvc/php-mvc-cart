@extends('layouts')

@section('content')
<section class="container">
	{{-- Demande des informations à l'utilisateur et enregistre la commande  --}}

	<?php if (!isset($_SESSION["user_id"])) { ?>


		<div class="column is-two-fifths">
			<h2 class="title is-3">Connexion</h2>
			<hr>
			<form action="/order/connect" method="POST">
				<div class="field">
					<label class="label">Nom</label>
					<div class="control">
						<input class="input" type="text" placeholder="Nom" name="last_name" require>
					</div>
				</div>

				<div class="field">
					<label class="label">Prénom</label>
					<div class="control">
						<input class="input" type="text" placeholder="Prénom" name="first_name" require>
					</div>
				</div>

				<div class="field is-grouped">
					<div class="control">
						<button type="submit" class="button is-link">Connexion</button>
					</div>
					<div class="control">
						<a href="/cart" class="button is-link is-light">Annuler</a>
					</div>

				</div>
				<a href="/register">Pas de compte ? Inscrivez vous !</a>
			</form>
		</div>

	<?php } else { ?>

		<h2 class="title is-2">Récapitulatif de votre commande</h2>

		<hr>
		<table class="table is-striped is-narrow is-hoverable is-fullwidth">
			<thead>
				<tr>
					<th>Articles</th>
					<th>Prix</th>
					<th>Quantité</th>
				</tr>
			</thead>
			<tbody>

				@foreach ($cart as $key => $quantity)
				<tr>
					<td>{{ $products[$key-1]->name }}</td>
					<td>{{ $products[$key-1]->price }}</td>
					<td>{{ $quantity }}</td>
				</tr>
				@endforeach

			</tbody>
			<tfoot>
				<tr>
					<th>Articles</th>
					<th>Prix</th>
					<th>Quantité</th>
					<th>
						{{-- Afficher le prix totals de tout les produits --}}
						<h3 class="subtitle is-5">Total (€) : {{ $total }} &euro;</h3>
						{{-- afficher le nombre de produits aux totals --}}
						<h3 class="subtitle is-5">Nombre de produits : {{ $nbproduct }}</h3>
						<div class="buttons">
							<a href="/order/add" class="button is-small is-success">Confirmer</a>
							<a href="/reset" class="button is-small is-danger">Annuler</a>
						</div>
					</th>
				</tr>
			</tfoot>
		</table>

	<?php } ?>

</section>
@endsection