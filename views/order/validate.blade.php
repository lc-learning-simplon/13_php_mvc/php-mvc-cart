@extends('layouts')

@section('content')
	<section class="container">
	{{-- 
				Commande est effectuer, récap sur ce qui à était commander (order, user)
				@Tips : fin de la session.
		--}}

		<h3 class="title is-3">Merci de votre confiance !</h3>
		<hr>

		<p>Vous allez être débité de {{$total}} €</p>
		<p>Votre référence de transaction : <strong>AZERTY</strong></p>
		<p>Un mail de confirmation vous a été envoyer à l'adresse : mail@bidule.com</p>
	</section>
@endsection