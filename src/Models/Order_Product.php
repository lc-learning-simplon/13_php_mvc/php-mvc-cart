<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model pour la table orders
 */
class Order_Product extends Model
{
	/**
	 * Les propriéter éditable de la table orders
	 * @var array
	 */
	protected $fillable = ['price', 'quantity'];
	protected $table = "order_product";

	/**
	 * désactive le timestamps
	 * @var boolean
	 */
	public $timestamps = false;
	

}