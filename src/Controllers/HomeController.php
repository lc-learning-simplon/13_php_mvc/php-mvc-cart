<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\Cart;

/**
 * Controller pour l'home
 */
class HomeController extends Controller {
	/**
	 * Affiche la page du magasin avec tout les produits
	 * @return view retourne la vue avec tout les produits
	 */
	public function index(){
		// return "Something Wrong ! Check your HomeController";
		return view('home', ['products' => Product::all(), "total" => Cart::total(), "user" => Customer::find($_SESSION["user_id"])]);
	}

	/**
	 * Destroy la session php
	 * @return view redirige vers la view home
	 */
	public function logout(){
		session_unset();
    	header('Location: /');
    	die();
	}
}