<?php 

namespace App\Controllers;

use Illuminate\Routing\Redirector;
use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Cart;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Order_Product;
/**
 * Controller pour gérer les commande et le panier
 */
class OrderController extends Controller {
	/**
	 * Affiche le résumer de la commande de l'ulisateur si il existe
	 * sinon créer le profile de l'utilisateur
	 * @return view retourne la vue order.create
	 */
	public function create(){
		return view('order.index', [
			"products" => Product::all(), 
			"cart" => $_SESSION["cart"], 
			"nbproduct" => Cart::count(),
			"total" => Cart::total(), 
			"user" => Customer::find($_SESSION["user_id"])]);
	}

	/**
	 * On Créer le client,
	 * Enregistre une commande et renvoie vers un récap de la commande
	 * @param  Request $request Récupère les données envoyer par le client
	 * @return redirige vers la récap de la commande
	 * @TIPS : https://laravel.com/docs/5.8/eloquent#inserting-and-updating-models
	 * 				 https://laravel.com/docs/5.8/eloquent-relationships#defining-relationships
	 * 				 pour la redirect:
	 * 				 	$redirect->to("[routeName]"); redirige vers une route
	 * 				  $redirect->back(); redirige vers la route précédente
	 */
	public function store(Request $request, Redirector $redirect){
		
		if(isset($_POST["last_name"])) {
			$last_name = $_POST["last_name"];
			$first_name = $_POST["first_name"];
			$address = $_POST["address"];
			$postcode = $_POST["postcode"];
			$phone = $_POST["phone"];
			$user = new Customer;
	
			$user->first_name = "$first_name";
			$user->last_name = "$last_name";
			$user->address = "$address";
			$user->postcode = "$postcode";
			$user->phone = "$phone";
	
			$user->save();
			$_SESSION["user_id"] = $user->id;
	
			return $redirect->to("/order");
		}

		else {
			$total = Cart::total();

			$order = new Order();
			$order->amount = $total;
			$order->shipped = 0;
			$order->created_at = date("Y-m-d");
			$order->customer_id = $_SESSION["user_id"];
			$order->save();

			foreach ($_SESSION["cart"] as $id => $quantity) {
				$order_product = new Order_Product();
				$order_product->product_id = $id;
				$order_product->order_id = $order->id;
				$order_product->price = Product::find($id)->price * $quantity;
				$order_product->quantity = $quantity;
				$order_product->save();
			}

			session_unset();

			return view("order/validate", ["total" => $total]);
		}
	}	

	public function connect(Request $request, Redirector $redirect){
		$last_name = $_POST["last_name"];
		$first_name = $_POST["first_name"];

		
		$user = Customer::where("last_name", "=", "$last_name")->first();


		$_SESSION["user_id"] = $user->id;

		return $redirect->back();
	}

	public function register(Request $request, Redirector $redirect){
		return view('order/register', ["total" => Cart::total(), 
		"user" => Customer::find($_SESSION["user_id"])]);
	}
	
}
