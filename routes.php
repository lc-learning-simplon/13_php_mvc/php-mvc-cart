<?php

use Illuminate\Routing\Router;

/** @var $router Router */

// les routes pour la page d'acceuil
$router->get('/', 'HomeController@index');
$router->get('/logout', 'HomeController@logout');

// les routes pour le panier
$router->get('/cart', 'CartController@index');
$router->post('/cart/add', 'CartController@store');

// les routes pour la commande
$router->get('/order', 'OrderController@create');
$router->get('/register', 'OrderController@register');
$router->post('/order/add', 'OrderController@store');
$router->get('/order/add', 'OrderController@store');
$router->post('/order/connect', 'OrderController@connect');

// les routes pour les produits
$router->get('/product/{id}', 'ProductController@show');

// les routes pour reset la session
$router->get('/reset', function() {
    session_unset();
    header('Location: /cart');
    die();
});


// catch-all route
$router->any('{any}', function () {
    return view('errors.404');
})->where('any', '(.*)');
